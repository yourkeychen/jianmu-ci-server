ALTER TABLE `workflow`
    add `failure_mode` varchar(45) DEFAULT NULL COMMENT '错误处理模式';

ALTER TABLE `workflow`
    add `created_time` datetime DEFAULT NULL COMMENT '创建时间';

ALTER TABLE `workflow_instance`
    add `failure_mode` varchar(45) DEFAULT NULL COMMENT '错误处理模式' AFTER `run_mode`;

ALTER TABLE `workflow_instance`
    add `suspended_time` datetime DEFAULT NULL COMMENT '挂起时间' AFTER `start_time`;
