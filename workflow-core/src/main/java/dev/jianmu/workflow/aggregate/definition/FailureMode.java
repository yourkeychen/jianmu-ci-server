package dev.jianmu.workflow.aggregate.definition;

/**
 * @author Ethan Liu
 * @class FailureMode
 * @description 失败处理模式
 * @create 2022-04-06 15:42
 */
public enum FailureMode {
    TERMINATE,
    IGNORE,
    MANUAL
}
